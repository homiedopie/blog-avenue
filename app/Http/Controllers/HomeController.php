<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function test()
    {
        $needle = 6;
        $haystack = ['apple', 'banana', 'apple', 'mango', 'banana', ['orange', 'banana'], ['orange', 'banana']];
//        mm/dd/yyyy, m/d/yyyy, mm/d/yyyy, m/dd/yyyy
//        /^(0?[1-9]|1[012])\/(0?[1-9]|[12][0-9]|3[01])\/\d{4}$/
        $result = $this->remove_duplicate_loop($haystack);
        dd($result);
    }

    private function search_number($needle = 0, $haystack = [])
    {
        $found = false;
        foreach ($haystack as $number) {
            if (is_array($number)) {
                $found = $this->search_number($needle, $number);
            } else if ($number == $needle) {
                $found = true;
            }

            if ($found) {
                return true;
            }
        }
        return false;
    }

    private function shuffle_elements($haystack = [])
    {
        $haystack_size = count($haystack);
        for($index = 0; $index < $haystack_size; ++$index) {
            $random_index = rand(0, $index);
            $current_element = $haystack[$index];
            $temp_element = null;

            //Shuffle first if array then return it
            if (is_array($haystack[$index])) {
                $haystack[$index] = $this->shuffle_elements($current_element);
            }

            $temp_element = $haystack[$index];
            $haystack[$index] = $haystack[$random_index];
            $haystack[$random_index] = $temp_element;
        }
        return $haystack;
    }

    //single dimensional
    private function remove_duplicates_simple($haystack = [])
    {
        //Only for string and integer array, not multidimensional
        return array_keys(array_flip($haystack));
    }
    //taking advantage of serialize
    private function remove_duplicates_multi_simple($haystack = [])
    {
        //Only for string and integer, multidimensional
        return array_map('unserialize', array_keys(array_flip(array_map('serialize', $haystack))));
    }

    private function remove_duplicate_loop($haystack = [], $search = null)
    {
        $result = [];
        foreach($haystack as &$item) {
            //Clean if array
            if (is_array($item)) {
                $item = $this->remove_duplicate_loop($item);
            }
            if ($search != null) {
                $result = false;
                if ($item == $search) {
                    //Once match return directly to break the loop
                    return true;
                }
            } else {
                $is_present = $this->remove_duplicate_loop($result, $item);
                if (!$is_present) {
                    $result[] = $item;
                }
            }
        }
        return $result;
    }

    private function validate_date_format($date = null)
    {
        return preg_match('/^(0?[1-9]|1[012])\/(0?[1-9]|[12][0-9]|3[01])\/(19|2\d)\d\d$/', $date);
    }
}
