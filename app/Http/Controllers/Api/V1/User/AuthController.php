<?php

namespace App\Http\Controllers\Api\V1\User;

use App\User;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $user_data = $request->only('first_name', 'last_name', 'password', 'email');
        if ($request->has('access_token')) {
            try {
                $oauth_user = \Socialite::driver('google')->userFromToken($request->get('access_token'));
                $user_data['google_provider_id'] = $oauth_user->getId();
                $user_data['email'] = $oauth_user->getEmail();
                $user_data['first_name'] = $oauth_user->user['name']['givenName'];
                $user_data['last_name'] = $oauth_user->user['name']['familyName'];

                $validator = \Validator::make($user_data, [
                    'first_name' => 'required|max:191',
                    'last_name' => 'required|max:191',
                    'email' => 'required|email|unique:users',
                    'google_provider_id' => 'required',
                ]);

                if ($validator->fails()) {
                    return response()
                        ->json([
                            'result' => false,
                            'register' => true,
                            'message' => 'Unable to register. Something went wrong in google user retrieval'
                        ], 422);
                }

                if (User::where('email', $user_data['email'])->first()) {
                    return response()
                        ->json([
                            'result' => false,
                            'register' => true,
                            'message' => 'User already exists!'
                        ], 422);
                }
            } catch (ClientException $exception) {
                return response()
                    ->json([
                        'result' => false,
                        'register' => true,
                        'message' => 'No user was retrieved in google sign in.'
                    ], 422);
            }
        } else {
            $this->validate($request, [
                'first_name' => 'required|max:191',
                'last_name' => 'required|max:191',
                'password' => 'required|confirmed',
                'email' => 'required|email|unique:users',
            ]);
        }

        $user = new User($user_data);
        $user->save();

        return response()
            ->json([
                'result' => true,
                'data' => [
                    'user' => $user,
                    'token' => \Auth::login($user)
                ],
                'message' => 'Registration successful!'
            ]);
    }

    public function login(Request $request)
    {
        $oauth = false;
        $email = null;
        $provider_id = null;

        if (!$request->has('access_token')) {
            $this->validate($request, [
                'password' => 'required',
                'email' => 'required|email',
            ]);
        } else {
            try {
                $oauth_user = \Socialite::driver('google')->userFromToken($request->get('access_token'));
                $email = $oauth_user->getEmail();
                $provider_id = $oauth_user->getId();
                $oauth = true;
            } catch (ClientException $exception) {
                return response()
                    ->json([
                        'result' => false,
                        'register' => true,
                        'message' => 'Wrong credentials!'
                    ], 401);
            }
        }

        $validate = $oauth ? ['email' => $email, 'google_provider_id' => $provider_id] : $request->only('email', 'password');
        if ($oauth) {
            $user = User::where('email', $email)->where('google_provider_id', $provider_id)->first();
            if (!$user) {
                $response = [
                    'result' => false,
                    'message' => 'Wrong credentials! Please register with your google account!'
                ];

                return response()
                    ->json($response, 401);
            }

            $token = \Auth::login($user);
        } else {
            $token = \Auth::attempt($validate);
        }

        \Log::info('validate credentials', [$validate, $token]);

        if ($token !== false) {
            return response()
                ->json([
                    'result' => true,
                    'data' => [
                        'user' => \Auth::user(),
                        'token' => $token
                    ],
                    'message' => 'Logged in successfully!'
                ]);
        } else {
            $response = [
                'result' => false,
                'message' => 'Wrong credentials!'
            ];
            if ($oauth) {
                $response['register'] = true;
                $response['message'] .= ' Please register with your google account!';
            }

            return response()
                ->json($response, 401);
        }
    }

    public function logout(Request $request)
    {
        if (\Auth::user()) {
            \Auth::logout();
        }
        return response()
            ->json([
                'result' => true,
                'message' => 'Logged out successfully!'
            ]);
    }
}
