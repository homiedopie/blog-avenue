<?php

namespace App\Http\Controllers\Api\V1\Blog;

use App\Http\Requests\UserPostFormRequest;
use App\Post;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource specific to only authenticated user.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $published_posts = $request->user()->posts()->latestPost()->with('user')->paginate(5);
        return response()
            ->json([
                'data' => [
                    'posts' => $published_posts
                ],
            ]);
    }

    /**
     * Display all listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all(Request $request)
    {
        $published_posts = Post::published()->latestPost()->presentPost()->with('user')->paginate(5);
        return response()
            ->json([
                'data' => [
                    'posts' => $published_posts
                ],
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:191',
            'content' => 'required',
        ]);

        $post_data = $request->only('title', 'content', 'published');
        $post_data['published_at'] = intval($post_data['published']) ? Carbon::now() : null;

        $post = new Post($post_data);
        $request->user()->posts()->save($post);

        return response()
            ->json([
                'result' => true,
                'data' => [
                    'post' => $post,
                ],
                'message' => 'You have successfully created the post!'
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        return response()
            ->json([
                'data' => [
                    'post' => $post
                ]
            ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $me
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $me)
    {
        return response()
            ->json([
                'data' => [
                    'post' => $me
                ]
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $me
     * @return \Illuminate\Http\Response
     */
    public function update(Post $me, UserPostFormRequest $request)
    {
        $this->validate($request, [
            'title' => 'required|max:191',
            'content' => 'required',
        ]);

        $post_data = $request->only('title', 'content', 'published_at');
        $post_data['published_at'] = intval($post_data['published_at']) ? Carbon::now() : null;
        $post_data['user_id'] = \Auth::user()->id;

        $me->fill($post_data);
        $me->save();

        return response()
            ->json([
                'result' => true,
                'data' => [
                    'post' => $me,
                ],
                'message' => 'You have successfully updated the post!'
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $me
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $me, UserPostFormRequest $request)
    {
        $me->delete();
        return response()
            ->json([
                'result' => true,
                'data' => [
                    'post' => $me,
                ],
                'message' => 'You have successfully deleted the post!'
            ]);
    }

    /**
     * Publish/unpublish the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function publish(Post $me, UserPostFormRequest $request)
    {
        $is_published = !!$me->published_at;
        $me->published_at = $is_published ? null : Carbon::now();
        $me->save();
        $me->load('user');

        return response()
            ->json([
                'result' => true,
                'data' => [
                    'post' => $me,
                ],
                'message' => 'You have successfully '.($is_published ? 'unpublished' : 'published').' the post!'
            ]);
    }
}
