<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'title', 'content', 'published_at'
    ];

    protected $hidden = [
        'user_id',
    ];

    protected $appends = [
        'html_content', 'date_published_humans', 'html_all_content'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Select only published posts
     * @param $query
     * @return mixed
     */
    public function scopePublished($query)
    {
        return $query->whereNotNull('published_at');
    }

    public function scopeLatestPost($query)
    {
        return $query->orderBy('published_at', 'desc');
    }

    public function scopePresentPost($query)
    {
        return $query->orderBy('published_at', '<=', Carbon::today());
    }

    public function getHtmlContentAttribute($value)
    {
        return \Markdown::convertToHtml(str_limit($this->content, 200));
    }

    public function getHtmlAllContentAttribute($value)
    {
        return \Markdown::convertToHtml($this->content);
    }

    public function getDatePublishedHumansAttribute($value)
    {
        return $this->published_at !== null ? Carbon::parse($this->published_at)->diffForHumans() : 'Not published';
    }
}
