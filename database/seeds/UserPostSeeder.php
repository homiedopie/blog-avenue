<?php

use Illuminate\Database\Seeder;

class UserPostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // 50 x 200
        // Save 50 users with 200 posts each
        factory(App\User::class, 50)->create()->each(function ($user) {
            $user->posts()->saveMany(factory(App\Post::class, 200)->make());
        });
    }
}
