<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'first_name' => $faker->firstName(),
        'last_name' => $faker->lastName,
        'email' => $faker->unique()->safeEmail,
        'password' => 'secret',
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Post::class, function (Faker\Generator $faker) {
    static $provider = null;
    if ($provider == null) {
        $faker->addProvider(new \DavidBadura\FakerMarkdownGenerator\FakerProvider($faker));
        $provider = true;
    }
    return [
        'title' => $faker->text(100),
        'content' => $faker->markdown(),
        'published_at' => $faker->optional($weight = 0.9)->dateTimeThisDecade($max = 'now'),
        'created_at' => $faker->optional($weight = 0.9)->dateTimeThisDecade($max = 'now'),
    ];
});