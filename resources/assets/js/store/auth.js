export default {
    state: {
        token : null,
    },
    initialize () {
        this.state.token = localStorage.getItem('token')
    },
    set(token) {
        localStorage.setItem('token', token)
        this.state.token = token
    },
    remove() {
      localStorage.removeItem('token')
      this.initialize()
    }
}