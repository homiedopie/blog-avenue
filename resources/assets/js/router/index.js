import Vue from 'vue'
import VueRouter from 'vue-router'

import Register from '../views/Auth/Register.vue'
import Login from '../views/Auth/Login.vue'
import BlogIndex from '../views/Blog/Index.vue'
import BlogShow from '../views/Blog/Show.vue'
import UserDashboard from '../views/User/Dashboard.vue'
import UserPost from '../views/User/Post/Show.vue'
import UserPostEdit from '../views/User/Post/Edit.vue'
import UserPostAdd from '../views/User/Post/Add.vue'

Vue.use(VueRouter)

const router = new VueRouter({
    routes : [
        {path: '/', component: BlogIndex},
        {path: '/post/:id', component: BlogShow},
        {path: '/dashboard', component: UserDashboard},
        {path: '/post/me/add', component: UserPostAdd},
        {path: '/post/me/:id', component: UserPost},
        {path: '/post/me/:id/edit', component: UserPostEdit},
        {path: '/register', component: Register},
        {path: '/login', component: Login}
    ]
})

export default router