import axios from 'axios'
import Auth from '../store/auth'

export function post(url, data) {
    return axios({
        method: 'POST',
        url: url,
        data: data,
        headers: {
            'Authorization' : `Bearer ${Auth.state.token}`
        }
    })
}

export function put(url, data) {
    return axios({
        method: 'PUT',
        url: url,
        data: data,
        headers: {
            'Authorization' : `Bearer ${Auth.state.token}`
        }
    })
}

export function get(url) {
    return axios({
        method: 'GET',
        url: url,
        headers: {
            'Authorization' : `Bearer ${Auth.state.token}`
        }
    })
}

export function del(url, data) {
    return axios({
        method: 'DELETE',
        url: url,
        data : data,
        headers: {
            'Authorization' : `Bearer ${Auth.state.token}`
        }
    })
}