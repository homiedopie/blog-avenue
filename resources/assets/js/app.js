import Vue from 'vue'
import App from './App.vue'
import Router from './router'

import FormError from './components/FormError.vue'
import Pagination from './components/Pagination.vue'
import VueMarkdown from 'vue-markdown'

Vue.component('form-error', FormError);
Vue.component('pagination', Pagination);
Vue.component('vue-markdown', VueMarkdown);

const app = new Vue({
    el: '#app',
    template : '<app></app>',
    components : { App },
    router : Router
});
