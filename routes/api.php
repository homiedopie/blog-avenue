<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
//auth:api
Route::group(['prefix' => 'v1', 'namespace' => 'Api\V1'], function ($router) {
    $router->get('posts/all', 'Blog\\PostsController@all')->name('posts.all');
    $router->group(['middleware' => 'auth:api'], function($router){
        $router->resource('posts/me', 'Blog\\PostsController');
    });
    $router->post('posts/me/{me}/publish', 'Blog\\PostsController@publish')->name('posts.publish');
    $router->get('posts/{post}', 'Blog\\PostsController@show')->name('posts.show');
    $router->post('user/login', 'User\\AuthController@login')->name('login');
    $router->post('user/register', 'User\\AuthController@register')->name('register');
    $router->post('user/logout', 'User\\AuthController@logout')->name('logout');
});

Route::get('/user/{user_id}', function ($user_id) {
    return ['token' => \Auth::generateTokenById($user_id)];
});
